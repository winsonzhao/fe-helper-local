new Vue({
    el: "#mainContainer",
    data: {
        unSavedNaotuKey: "NAOTU-UN-SAVED-KEY",
        mySavedNaotuListKey: "NAOTU-MY-SAVED-KEY",
        mySavedNaotuList: [],
        curNaotu: {},
        oldMinderData: null,
        showSavedNaotuList: !1,
        naotuTips: ""
    },
    mounted: function() {
        let t = this;
        angular.module("kityminderContainer", ["kityminderEditor"]).controller("MainController", ["$scope", function(e) {
            e.initEditor = function(e, a) {
                window.editor = e,
                window.minder = a,
                t.listenChange()
            }
        }
        ]),
        angular.bootstrap(document.body, ["kityminderContainer"]),
        this.mySavedNaotuList = JSON.parse(localStorage.getItem(this.mySavedNaotuListKey) || "[]");
        let e = localStorage.getItem(this.unSavedNaotuKey) || "";
        e && (e = JSON.parse(e)).data.root.children.length && (this.naotuTips = `上次有一个编辑未保存的脑图【${e.title}】，可以<span class="x-load">继续编辑</span>`),
        window.addEventListener("keydown", t=>{
            (t.metaKey || t.ctrlKey) && 83 === t.keyCode && (this.saveNaotu(),
            t.preventDefault(),
            t.stopPropagation())
        }
        , !1)
    },
    methods: {
        listenChange: function() {
            window.editor.minder.on("contentchange", ()=>{
                const t = editor.minder.exportJson();
                json_diff(this.oldMinderData, t).length > 0 && t.root.children.length && (this.oldMinderData = t,
                localStorage.setItem(this.unSavedNaotuKey, JSON.stringify(this.getCurrentNaotu())),
                this.naotuTips = `自动保存于 ${this.dateFormat(1 * new Date)}`)
            }
            )
        },
        loadUnSaved: function() {
            this.$refs.tipsBar.querySelector("span.x-load") && (this.curNaotu = JSON.parse(localStorage.getItem(this.unSavedNaotuKey) || "{}"),
            editor.minder.importJson(this.curNaotu.data),
            this.naotuTips = "")
        },
        getCurrentNaotu: function() {
            return {
                id: this.curNaotu.id || `fh_${1 * new Date}`,
                created_at: this.curNaotu.created_at || 1 * new Date,
                updated_at: 1 * new Date,
                title: editor.minder.getMinderTitle(),
                data: editor.minder.exportJson()
            }
        },
        newNaotu: function() {
            editor.minder.getMinderTitle();
            editor.minder.importData("text", "中心主题")
        },
        saveNaotu: function() {
            this.curNaotu = this.getCurrentNaotu(),
            this.mySavedNaotuList.some(t=>t.id === this.curNaotu.id && (t.title = this.curNaotu.title,
            t.data = this.curNaotu.data,
            t.updated_at = this.curNaotu.updated_at,
            !0)) || this.mySavedNaotuList.unshift(this.curNaotu),
            localStorage.setItem(this.mySavedNaotuListKey, JSON.stringify(this.mySavedNaotuList)),
            localStorage.removeItem(this.unSavedNaotuKey),
            this.naotuTips = `脑图【${this.curNaotu.title}】已保存成功！`
        },
        importNaotu: function() {
            let t = document.createElement("input");
            t.type = "file",
            t.accept = "application/json",
            t.style.cssText = "position:absolute;top:-100px;left:-100px",
            t.addEventListener("change", t=>{
                let e = new FileReader;
                e.onload = (t=>{
                    let e = t.target.result;
                    try {
                        let t = JSON.parse(e);
                        t instanceof Array || (t = [t]);
                        let a = 0
                          , i = 0;
                        t.map(t=>(this.mySavedNaotuList.some(e=>e.id === t.id && (e.updated_at < t.updated_at && (e = t,
                        a++),
                        t._replaced = !0,
                        !0)),
                        t)).filter(t=>!t._replaced).map(t=>{
                            this.mySavedNaotuList.unshift(t),
                            i++
                        }
                        ),
                        this.naotuTips = `文件累计${t.length}条，共导入${a + i}条，其中替换${a}条，新增${i}条`,
                        this.curNaotu = t[0],
                        editor.minder.importJson(this.curNaotu.data)
                    } catch (t) {}
                }
                ),
                e.readAsText(t.target.files[0])
            }
            , !1),
            document.body.appendChild(t),
            t.click()
        },
        exportNaotu: function(t, e) {
            if ("png" === t)
                editor.minder.exportData("png").then(t=>{
                    let e = document.createElement("a");
                    e.setAttribute("download", `FH-${editor.minder.getMinderTitle()}.png`),
                    e.setAttribute("href", t),
                    e.style.cssText = "position:absolute;top:-1000px;left:-1000px;",
                    document.body.appendChild(e),
                    e.click(),
                    e.remove()
                }
                );
            else if ("json" === t) {
                let t = null
                  , a = `FeHelper-Naotu-${1 * new Date}.json`;
                if ("all" === e)
                    t = new Blob([JSON.stringify(this.mySavedNaotuList)],{
                        type: "application/octet-stream"
                    });
                else {
                    let e = this.getCurrentNaotu();
                    t = new Blob([JSON.stringify(e)],{
                        type: "application/octet-stream"
                    }),
                    a = `FeHelper-Naotu-${e.title}.json`
                }
                let i = document.createElement("a");
                i.setAttribute("download", a),
                i.setAttribute("href", URL.createObjectURL(t)),
                i.style.cssText = "position:absolute;top:-1000px;left:-1000px;",
                document.body.appendChild(i),
                i.click(),
                i.remove()
            }
        },
        myNaotu: function() {
            this.showSavedNaotuList = !this.showSavedNaotuList
        },
        editNaotu: function(t) {
            this.mySavedNaotuList.some(e=>e.id === t && (this.curNaotu = e,
            editor.minder.importJson(e.data),
            this.showSavedNaotuList = !1,
            !0))
        },
        deleteNaotu: function(t) {
            this.mySavedNaotuList.some((e,a)=>e.id === t && (this.mySavedNaotuList.splice(a, 1),
            !0)),
            localStorage.setItem(this.mySavedNaotuListKey, JSON.stringify(this.mySavedNaotuList))
        },
        dateFormat: function(t) {
            let e = "yyyy-MM-dd HH:mm:ss"
              , a = function(t, e) {
                let a = ""
                  , i = t < 0
                  , o = String(Math.abs(t));
                return o.length < e && (a = new Array(e - o.length + 1).join("0")),
                (i ? "-" : "") + a + o
            }
              , i = function(t, a) {
                e = e.replace(t, a)
            }
              , o = (t = new Date(t)).getFullYear()
              , n = t.getMonth() + 1
              , s = t.getDate()
              , r = t.getHours()
              , d = t.getMinutes()
              , u = t.getSeconds()
              , l = t.getMilliseconds();
            return i(/yyyy/g, a(o, 4)),
            i(/yy/g, a(parseInt(o.toString().slice(2), 10), 2)),
            i(/MM/g, a(n, 2)),
            i(/M/g, n),
            i(/dd/g, a(s, 2)),
            i(/d/g, s),
            i(/HH/g, a(r, 2)),
            i(/H/g, r),
            i(/hh/g, a(r % 12, 2)),
            i(/h/g, r % 12),
            i(/mm/g, a(d, 2)),
            i(/m/g, d),
            i(/ss/g, a(u, 2)),
            i(/s/g, u),
            i(/SSS/g, a(l, 3)),
            i(/S/g, l),
            e
        }
    }
});
