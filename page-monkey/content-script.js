window.pagemonkeyContentScript = function() {
    chrome.runtime.sendMessage({
        type: "fh-dynamic-any-thing",
        params: {
            url: location.href,
            tabId: window.__FH_TAB_ID__
        },
        func: ((t,e)=>{
            try {
                const r = "PAGE-MODIFIER-LOCAL-STORAGE-KEY";
                let a = e=>{
                    e.filter(t=>!t.mDisabled).forEach(e=>{
                        let r = null
                          , a = e.mPattern.match(/^\/(.*)\/(.*)?$/);
                        if (a && (!a[2] || a[2] && !/[^igm]*/i.test(a[2])))
                            e.mPattern = new RegExp(a[1],a[2] || ""),
                            r = e.mPattern.test(t.url) && e;
                        else if (e.mPattern.indexOf("*") > -1)
                            e.mPattern.startsWith("*://") ? e.mPattern = e.mPattern.replace("*://", "(http|https|file)://") : e.mPattern.indexOf("://") < 0 && (e.mPattern = "(http|https|file)://" + e.mPattern),
                            e.mPattern = new RegExp("^" + e.mPattern.replace(/\./g, "\\.").replace(/\//g, "\\/").replace(/\*/g, ".*").replace(/\?/g, "\\?") + "$"),
                            r = e.mPattern.test(t.url) && e;
                        else {
                            let a = [e.mPattern, `${e.mPattern}/`];
                            e.mPattern.startsWith("http://") || e.mPattern.startsWith("https://") || (a = a.concat([`http://${e.mPattern}`, `http://${e.mPattern}/`, `https://${e.mPattern}`, `https://${e.mPattern}/`])),
                            a.includes(t.url) && (r = e)
                        }
                        if (r) {
                            let e = "(" + (t=>{
                                let e = ()=>{
                                    let e = document.createElement("script");
                                    e.type = "text/javascript",
                                    e.textContent = t.mScript,
                                    document.body.appendChild(e),
                                    parseInt(t.mRefresh) && setTimeout(()=>{
                                        location.reload(!0)
                                    }
                                    , 1e3 * parseInt(t.mRefresh))
                                }
                                ;
                                window._fhImportJs = (t=>new Promise(e=>{
                                    let r = document.createElement("script");
                                    r.type = "text/javascript",
                                    r.src = t,
                                    r.addEventListener("load", e, !1),
                                    document.body.appendChild(r)
                                }
                                ));
                                let r = (t.mRequireJs || "").split(/[\s,，]+/).filter(t=>t.length);
                                if (r.length) {
                                    let t = Array.from(new Set(r)).map(t=>window._fhImportJs(t));
                                    Promise.all(t).then(e)
                                } else
                                    e()
                            }
                            ).toString() + `)(${JSON.stringify(r)})`;
                            "function" == typeof injectScriptIfTabExists ? injectScriptIfTabExists(t.tabId, {
                                code: e,
                                allFrames: !1
                            }) : chrome.tabs.executeScript(t.tabId, {
                                code: e,
                                allFrames: !1
                            })
                        }
                    }
                    )
                }
                ;
                chrome.storage.local.get(r, e=>{
                    let n, l = !1;
                    if (e && e[r] ? n = e[r] || "[]" : (n = localStorage.getItem(r) || "[]",
                    l = !0),
                    t && t.url && a(JSON.parse(n)),
                    l) {
                        let t = {};
                        t[r] = n,
                        chrome.storage.local.set(t)
                    }
                }
                )
            } catch (t) {
                e && e(null)
            }
            return !0
        }
        ).toString()
    })
}
;
