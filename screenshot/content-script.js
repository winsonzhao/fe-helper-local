window.screenshotContentScript = function(t) {
    let e = []
      , n = {}
      , o = 1e5
      , i = 4e4
      , l = o * i;
    function r(t) {
        let e = t.split("?")[0].split("#")[0];
        return "fehelper" + (e = e ? "-" + (e = e.replace(/^https?:\/\//, "").replace(/[^A-z0-9]+/g, "-").replace(/-+/g, "-").replace(/^[_\-]+/, "").replace(/[_\-]+$/, "")) : "") + "-" + Date.now() + ".png"
    }
    window.addScreenShot = function(t, n) {
        let r = new Image;
        r.onload = function() {
            if (t.image = {
                width: r.width,
                height: r.height
            },
            t.windowWidth !== r.width) {
                let e = r.width / t.windowWidth;
                t.x *= e,
                t.y *= e,
                t.totalWidth *= e,
                t.totalHeight *= e
            }
            e.length || Array.prototype.push.apply(e, function(t, e) {
                let n, r, c, a, d, s = e > o || t > o || e * t > l, u = t > e, f = s ? u ? o : i : t, h = s ? u ? i : o : e, m = Math.ceil(t / f), w = Math.ceil(e / h), p = 0, g = [];
                for (n = 0; n < w; n++)
                    for (r = 0; r < m; r++)
                        (c = document.createElement("canvas")).width = r === m - 1 && t % f || f,
                        c.height = n === w - 1 && e % h || h,
                        a = r * f,
                        d = n * h,
                        g.push({
                            canvas: c,
                            ctx: c.getContext("2d"),
                            index: p,
                            left: a,
                            right: a + c.width,
                            top: d,
                            bottom: d + c.height
                        }),
                        p++;
                return g
            }(t.totalWidth, t.totalHeight)),
            function(t, e, n, o, i) {
                let l = t + n
                  , r = e + o;
                return i.filter(function(n) {
                    return t < n.right && l > n.left && e < n.bottom && r > n.top
                })
            }(t.x, t.y, r.width, r.height, e).forEach(function(e) {
                e.ctx.drawImage(r, t.x - e.left, t.y - e.top)
            }),
            1 === t.complete && c.success()
        }
        ,
        r.src = n
    }
    ;
    let c = {
        success: function() {
            n = {
                pageInfo: t.tabInfo,
                resultTab: t.captureInfo.resultTab,
                filename: r(t.tabInfo.url),
                dataUris: e.map(t=>t.canvas.toDataURL())
            },
            chrome.runtime.sendMessage({
                type: "fh-dynamic-any-thing",
                params: n,
                func: ((t,e)=>{
                    function n(e) {
                        t.fileSystemUrl = e;
                        let n = e=>{
                            chrome.tabs.sendMessage(e.id, {
                                type: "page-screenshot-done",
                                data: t
                            })
                        }
                        ;
                        t.resultTab ? chrome.tabs.update(t.resultTab, {
                            highlighted: !0,
                            active: !0
                        }, n) : chrome.tabs.create({
                            url: "dynamic/index.html?tool=screenshot",
                            active: !0
                        }, t=>{
                            setTimeout((t=>()=>n(t))(t), 500)
                        }
                        )
                    }
                    !function(e) {
                        let o = e.map(function(t) {
                            let e = atob(t.split(",")[1])
                              , n = t.split(",")[0].split(":")[1].split(";")[0]
                              , o = new ArrayBuffer(e.length)
                              , i = new Uint8Array(o);
                            for (let t = 0; t < e.length; t++)
                                i[t] = e.charCodeAt(t);
                            return new Blob([o],{
                                type: n
                            })
                        })
                          , i = 0
                          , l = o.length;
                        !function e() {
                            !function(t, e, n, o, i) {
                                e = ((t,e)=>{
                                    if (!e)
                                        return t;
                                    let n = t.split(".")
                                      , o = n.pop();
                                    return n.join(".") + "-" + (e + 1) + "." + o
                                }
                                )(e, n);
                                let l = `filesystem:chrome-extension://${chrome.i18n.getMessage("@@extension_id")}/temporary/${e}`
                                  , r = t.size + 512;
                                (window.requestFileSystem || window.webkitRequestFileSystem)(window.TEMPORARY, r, function(n) {
                                    n.root.getFile(e, {
                                        create: !0
                                    }, function(e) {
                                        e.createWriter(function(e) {
                                            e.onwriteend = (()=>o(l)),
                                            e.write(t)
                                        }, i)
                                    }, i)
                                }, i)
                            }(o[i], t.filename, i, function(t) {
                                ++i >= l ? n(t) : e()
                            }, n)
                        }()
                    }(t.dataUris),
                    e && e()
                }
                ).toString()
            })
        },
        fail: t=>{
            alert(t && t.message || t || "稍后尝试刷新页面重试!")
        }
        ,
        progress: e=>{
            let n = parseInt(100 * e, 10) + "%";
            return document.title = `进度：${n}...`,
            "100%" === n && setTimeout(()=>{
                document.title = t.tabInfo.title
            }
            , 800),
            !0
        }
    };
    function a(t) {
        return Math.max.apply(Math, t.filter(function(t) {
            return t
        }))
    }
    return function() {
        if (!function(t) {
            let e, n, o = ["http://*/*", "https://*/*", "ftp://*/*", "file://*/*"], i = [/^https?:\/\/chrome.google.com\/.*$/];
            for (n = i.length - 1; n >= 0; n--)
                if (i[n].test(t))
                    return !1;
            for (n = o.length - 1; n >= 0; n--)
                if ((e = new RegExp("^" + o[n].replace(/\*/g, ".*") + "$")).test(t))
                    return !0;
            return !1
        }(t.tabInfo.url))
            return c.fail("invalid url");
        let e = document.body
          , n = e ? e.style.overflowY : ""
          , o = window.scrollX
          , i = window.scrollY
          , l = document.documentElement.style.overflow;
        e && (e.style.overflowY = "visible");
        let r, d, s = [document.documentElement.clientWidth, e ? e.scrollWidth : 0, document.documentElement.scrollWidth, e ? e.offsetWidth : 0, document.documentElement.offsetWidth], u = [document.documentElement.clientHeight, e ? e.scrollHeight : 0, document.documentElement.scrollHeight, e ? e.offsetHeight : 0, document.documentElement.offsetHeight], f = a(s), h = a(u), m = window.innerWidth, w = window.innerHeight, p = [], g = w - (w > 200 ? 200 : 0), y = m, b = h - w, x = !1;
        if (f <= y + 1 && (f = y),
        document.documentElement.style.overflow = "hidden",
        "visible" === t.captureInfo.captureType)
            p = [window.scrollX, window.scrollY],
            f = window.innerWidth,
            h = window.innerHeight,
            x = !0;
        else
            for (; b > -g; ) {
                for (r = 0; r < f; )
                    p.push([r, b]),
                    r += y;
                b -= g
            }
        function v() {
            document.documentElement.style.overflow = l,
            e && (e.style.overflowY = n),
            window.scrollTo(o, i)
        }
        d = p.length,
        function e() {
            if (!p.length)
                return v();
            let n = p.pop()
              , o = n[0]
              , i = n[1]
              , l = 1
              , r = 0
              , a = 0;
            x || (window.scrollTo(o, i),
            l = (d - p.length) / d,
            r = window.scrollX,
            a = window.scrollY);
            let s = {
                x: r,
                y: a,
                complete: l,
                windowWidth: m,
                totalWidth: f,
                totalHeight: h,
                devicePixelRatio: window.devicePixelRatio,
                tabInfo: t.tabInfo,
                captureInfo: t.captureInfo
            };
            window.setTimeout(function() {
                let t = window.setTimeout(v, 1250);
                c.progress(s.complete),
                window.captureCallback = function() {
                    window.clearTimeout(t),
                    1 !== s.complete ? e() : v()
                }
                ,
                chrome.runtime.sendMessage({
                    type: "fh-dynamic-any-thing",
                    params: s,
                    func: ((t,e)=>(chrome.tabs.captureVisibleTab(null, {
                        format: "png",
                        quality: 100
                    }, e=>{
                        chrome.tabs.executeScript(t.tabInfo.id, {
                            code: `window.addScreenShot(${JSON.stringify(t)},'${e}');`
                        })
                    }
                    ),
                    chrome.tabs.executeScript(t.tabInfo.id, {
                        code: "window.captureCallback();"
                    }),
                    !0)).toString()
                })
            }, 150)
        }()
    }
}
;
