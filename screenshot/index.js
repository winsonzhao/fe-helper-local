new Vue({
    el: "#pageContainer",
    data: {
        tabList: [],
        capturedImage: "",
        imageHTML: "",
        defaultFilename: Date.now() + ".png"
    },
    mounted: function() {
        this.updateTabList(),
        this.bindEvent()
    },
    methods: {
        bindEvent: function() {
            chrome.tabs.onCreated.addListener(t=>{
                /^http(s)?:\/\//.test(t.url) && this.tabList.push({
                    id: t.id,
                    title: t.title,
                    url: t.url
                })
            }
            ),
            chrome.tabs.onUpdated.addListener((t,e,i)=>{
                this.tabList.some((e,s)=>e.id === t && (e.title = i.title,
                e.url = i.url,
                !0))
            }
            ),
            chrome.tabs.onRemoved.addListener(t=>{
                this.tabList.some((e,i)=>e.id === t && (this.tabList.splice(i, 1),
                !0))
            }
            ),
            chrome.runtime.onMessage.addListener((t,e,i)=>{
                if ("page-screenshot-done" === t.type) {
                    let e = t.data;
                    e && e.fileSystemUrl && (this.capturedImage = e.fileSystemUrl,
                    this.imageHTML = `<img class="img-result" src="${this.capturedImage}" />`,
                    this.defaultFilename = e.filename,
                    this.$nextTick(()=>{
                        this.$refs.resultBox.scrollIntoView()
                    }
                    ),
                    chrome.tabs.get(e.pageInfo.id, t=>{
                        this.tabList.some(e=>e.id === t.id && (e.title = t.title,
                        !0))
                    }
                    ))
                }
                return i && i(),
                !0
            }
            )
        },
        updateTabList: function() {
            chrome.tabs.query({
                windowId: chrome.windows.WINDOW_ID_CURRENT
            }, t=>{
                this.tabList = t.filter(t=>/^http(s)?:\/\//.test(t.url)).map(t=>({
                    id: t.id,
                    title: t.title,
                    url: t.url
                }))
            }
            )
        },
        goCapture: function(t, e) {
            chrome.tabs.getCurrent(i=>{
                chrome.tabs.query({
                    windowId: chrome.windows.WINDOW_ID_CURRENT
                }, s=>{
                    s.some(e=>e.id === t) ? chrome.tabs.update(t, {
                        highlighted: !0,
                        active: !0
                    }, t=>{
                        chrome.tabs.executeScript(t.id, {
                            code: "(" + function(t, e) {
                                chrome.runtime.sendMessage({
                                    type: "fh-dynamic-any-thing",
                                    params: {
                                        tabInfo: t,
                                        captureInfo: e
                                    },
                                    func: ((t,e)=>{
                                        try {
                                            e && e(t)
                                        } catch (t) {
                                            e && e(null)
                                        }
                                        return !0
                                    }
                                    ).toString()
                                }, t=>{
                                    let e = window.screenshotContentScript;
                                    e && e(t)()
                                }
                                )
                            }
                            .toString() + ")(" + JSON.stringify(t) + "," + JSON.stringify({
                                resultTab: i.id,
                                captureType: e
                            }) + ")",
                            allFrames: !1
                        })
                    }
                    ) : alert("页面已关闭")
                }
                )
            }
            )
        },
        save: function() {
            chrome.permissions.request({
                permissions: ["downloads"]
            }, t=>{
                t ? chrome.downloads.download({
                    url: this.capturedImage,
                    saveAs: !0,
                    conflictAction: "overwrite",
                    filename: this.defaultFilename
                }) : alert("必须接受授权，才能正常下载！")
            }
            )
        }
    }
});
