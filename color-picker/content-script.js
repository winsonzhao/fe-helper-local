window.colorpickerContentScript = function() {
    let e = window.FeHelper || {};
    e.elemTool = {
        elm: function(e, t, i, o) {
            var r, n, l = document.createElement(e);
            if (t && (t.event || t.events)) {
                var c = t.event || t.events;
                if ("string" == typeof c[0])
                    l.addEventListener(c[0], c[1], c[2]);
                else if (c.length)
                    for (r = 0,
                    n = c.length; r < n; r++)
                        l.addEventListener(c[r][0], c[r][1], c[r][2])
            }
            for (r in t)
                "event" == r.substring(0, 5) || ("checked" == r || "selected" == r ? t[r] && l.setAttribute(r, r) : l.setAttribute(r, t[r]));
            if (i)
                for (r = 0,
                n = i.length; r < n; r++)
                    i[r] && l.appendChild(i[r]);
            return o && this.insertNode(l, o),
            l
        },
        txt: function(e) {
            return document.createTextNode(e)
        },
        ent: function(e) {
            return document.createTextNode(this.unescapeHtml(e))
        },
        paragraphs: function(e) {
            for (var t = e.split("\n"), i = [], o = 0, r = t.length; o < r; o++)
                i.push(elemTool.elm("p", {}, [elemTool.ent(t[o])]));
            return i
        },
        insertNode: function(e, t, i) {
            t || (t = document.body),
            i && i.parentNode == t ? t.insertBefore(e, i) : t.appendChild(e)
        },
        insertNodes: function(e, t, i) {
            if ("array" != typeof e)
                this.insertNode(e, t, i);
            else
                for (var o = 0, r = e.length; o < r; o++)
                    this.insertNode(e[o], t, i, !0)
        },
        empty: function(e) {
            for (; e.lastChild; )
                e.removeChild(e.lastChild)
        },
        unescapeHtml: function(e) {
            if (e.length < 1)
                return e;
            var t = document.createElement("div");
            e = e.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gim, ""),
            t.innerHTML = e;
            var i = t.childNodes[0].nodeValue;
            return this.empty(t),
            i
        }
    },
    e.ColorPicker = function() {
        if (document.documentElement instanceof HTMLElement) {
            var t, i = "fehelper-colorpicker-box", o = "fehelper-colorpicker-result", r = !1, n = !1, l = "F00BAF", c = null, a = 0, d = 0, s = !1, u = !1, h = !1, m = "1px solid #666", p = "", f = 0, g = 0, v = document.createElement("canvas"), A = v.getContext("2d"), y = 1, x = 1, k = !1, w = 0, b = 0, I = 150;
            return function(e) {
                e.setPickerImage ? n.src = e.pickerImage : (X(),
                F())
            }
        }
        function E(e) {
            return document.getElementById(e)
        }
        function C(e, t, i) {
            return o = R(e) + R(t) + R(i),
            h ? o.toLowerCase() : o;
            var o
        }
        function R(e) {
            return null == e ? "00" : 0 == (e = parseInt(e)) || isNaN(e) ? "00" : (e = Math.max(0, e),
            e = Math.min(e, 255),
            e = Math.round(e),
            "0123456789ABCDEF".charAt((e - e % 16) / 16) + "0123456789ABCDEF".charAt(e % 16))
        }
        function B(e) {
            for (; e.lastChild; )
                e.removeChild(e.lastChild)
        }
        function T() {
            n.style.height = "auto",
            n.style.width = innerWidth + "px",
            y = n.naturalWidth / innerWidth,
            x = n.naturalHeight / innerHeight,
            v.width = n.naturalWidth,
            v.height = n.naturalHeight,
            A.drawImage(n, 0, 0),
            setTimeout(function() {
                k = !1,
                n.style.visibility = "visible",
                r.style.visibility = "visible",
                document.body.style.cursor = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhCAYAAABX5MJvAAAA8ElEQVRYCe1WAQ6DIAwsm//e9rP9jOU2z0CpyWhlcQkkpoCld15rVcQ5cs5ZRLZrXbuiXVynDj40SVDQkBKfshChZdBe6yaRUkolmF6X934xxxsSGm4lQqjq8CRBQa6cBCwK9Bk4LzMdVO8USizW129k47HwFtX43i0YjiOIkIDGRGV/2/EezKGydxHBZY2btWntbT8mIMRhOe7s7RFo3IvYFSZqonEekQqAIO5KpMJETVRfw+rugIWFd4pXdJI4MtttZXdGn+mgYP+vBBoPnoaWT9Zr3UpoYL3uIeImARA2W9oe4NI3RKIMFJlPElTvBRTxXFcOwvQSAAAAAElFTkSuQmCC) 16 16,crosshair",
                D()
            }, 255)
        }
        function M(e) {
            e.target.select()
        }
        function Q() {
            B(r),
            e.elemTool.elm("div", {}, [e.elemTool.elm("input", {
                type: "text",
                size: 7,
                style: "width:80px;height:20px;line-height:20px;font-size:10pt;border:" + m,
                id: "fehelper-colorpicker-cphexvl",
                value: "#" + l,
                event: ["mouseover", M]
            }), e.elemTool.elm("img", {
                style: "width:20px;height:20px;position:absolute;top:-10px;right:-10px;cursor:pointer;",
                src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAACwklEQVQ4Ea1US0tbQRg9kwmp8YXBi7hKyMZAcaFuFN26daFEUg1BEF24FQr9D/0BIqhbDe0/0I0LceNCQSIVIcZdLkrER2LUm6+caeZiEmkp7cDlMnfOOfM9zneB/7zUn/REJAEgXsfllVI/fscJvncoIhrAZwArrutGb25uoJRCb28vROQKwBqAr0opr5nfEqGIpGq12s7h4SH29vZQKpUMR2uN9vZ29PX1YWJiAsPDw7zkk1Iq+1a0QVBEkuVy+dvW1hYuLi58HKMLBoPo6uoyb+6j0ShmZmYQDodnlVLfLdgXFJGQ53nVjY0NnJ+fo1arMT2Do0BbWxs6OjoQCASMqOd5iMViSKVS0Fp/UEo9ExywygBWj4+PUSwWTWpDQ0MYHx/3jylE4cHBQSwuLiIUCqFQKODk5ISYVQt8K7jCujE1RtPf349kMomRkRGLRSKRwNTUlMng+dkEBHLYPAsyXRaRj9fX19H7+3sTBQ+Pjo5Miul0Gg8PD+aS6elp831/f9/ycXd3B3KpoZTKWdvEb29vfRBT4yKR0S4vL5uLTk9Psbu7619qCeQ6jkOv5t6mbM/Nm6J8WFfWj4sXsFG2WQ2E+sYK5nt6elrOHcfB/Py8sdDl5SUWFhZokxbROjdPASPI3B3Hueru7vZFCZqbm2N9sL29jc3NTVMv1pQdtpHSm+RSwxesq6yNjY35grQMm5TNZvHy8oKnpyesr6/Tc6bz3L++vmJ0dJQcjqJZLcamAP1F+7B2lUoFj4+PRpBRdXZ2mm9kDwwMYGlp6X1j0+la61lag2PF26vVqpkYToVdtBD38XgcmUyGYhy9X6YE4EdoCfw5eJ63wwk4ODiA67ool8tGhJhIJILJyUmwPFrrlp+D1Wl4c65F5IuIFFzXlbOzM8nlclIsFoXf6mehBlJ90xJhM+hvf7DN/H/e/wRZ4k9klRmUggAAAABJRU5ErkJggg==",
                alt: "Close",
                title: "[esc]键可直接关闭",
                id: "fehelper-colorpicker-exitbtn",
                event: ["click", S, !0]
            })], r),
            E("fehelper-colorpicker-cphexvl") && E("fehelper-colorpicker-cphexvl").select(),
            Y()
        }
        function F() {
            u ? (l,
            u = !1,
            B(r)) : (u = !0,
            Q())
        }
        function S() {
            setTimeout(L, 500)
        }
        function L() {
            s = !1,
            u = !1,
            document.removeEventListener("mousemove", U),
            removeEventListener("scroll", V),
            removeEventListener("resize", V),
            removeEventListener("keyup", N),
            W(),
            clearTimeout(w)
        }
        function W() {
            document.body && (n = E(i),
            r = E(o),
            n && document.body.removeChild(n),
            r && document.body.removeChild(r),
            n = !1,
            r = !1,
            document.body.style.cursor = "")
        }
        function N(e) {
            s && (27 == e.keyCode ? S() : 82 == e.keyCode || 74 == e.keyCode ? V() : 13 == e.keyCode && F())
        }
        function U(e) {
            s && (u || (f = e.pageX - pageXOffset,
            g = e.pageY - pageYOffset,
            a = Math.round(f * y),
            d = Math.round(g * x),
            D()))
        }
        function V(e) {
            s && (r.style.visibility = "hidden",
            n.style.visibility = "hidden",
            clearTimeout(w),
            w = setTimeout(function() {
                O()
            }, 250))
        }
        function X() {
            return L(),
            r ? P() : (W(),
            n = e.elemTool.elm("img", {
                id: i,
                src: p,
                style: "position:fixed;max-width:none!important;max-height:none!important;top:0px;left:0px;margin:0px;padding:0px;overflow:hidden;z-index:2147483646;",
                events: [["click", F, !0], ["load", T]]
            }, [], document.body),
            r = e.elemTool.elm("div", {
                id: o,
                style: "position:fixed;min-width:30px;max-width:300px;box-shadow:2px 2px 2px #666;border:" + m + ";border-radius:5px;z-index:2147483646;cursor:default;padding:10px;text-align:center;"
            }, [], document.body),
            document.addEventListener("mousemove", U),
            addEventListener("keyup", N),
            addEventListener("scroll", V),
            addEventListener("resize", V),
            H(),
            P(),
            !1)
        }
        function P() {
            return !!s || (r.style.visibility = "hidden",
            n.style.visibility = "hidden",
            u && F(),
            document.body.style.cursor = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhCAYAAABX5MJvAAAA8ElEQVRYCe1WAQ6DIAwsm//e9rP9jOU2z0CpyWhlcQkkpoCld15rVcQ5cs5ZRLZrXbuiXVynDj40SVDQkBKfshChZdBe6yaRUkolmF6X934xxxsSGm4lQqjq8CRBQa6cBCwK9Bk4LzMdVO8USizW129k47HwFtX43i0YjiOIkIDGRGV/2/EezKGydxHBZY2btWntbT8mIMRhOe7s7RFo3IvYFSZqonEekQqAIO5KpMJETVRfw+rugIWFd4pXdJI4MtttZXdGn+mgYP+vBBoPnoaWT9Zr3UpoYL3uIeImARA2W9oe4NI3RKIMFJlPElTvBRTxXFcOwvQSAAAAAElFTkSuQmCC) 16 16,crosshair",
            s = !0,
            setTimeout(O, 1),
            !1)
        }
        function Y() {
            r && (r.style.top = g + 8 + "px",
            r.style.left = f + 8 + "px",
            r.clientWidth + r.offsetLeft + 24 > innerWidth && (r.style.left = f - 8 - r.clientWidth + "px"),
            r.clientHeight + r.offsetTop + 24 > innerHeight && (r.style.top = g - 8 - r.clientHeight + "px"))
        }
        function D(i) {
            if (s) {
                Y();
                var o, n = A.getImageData(a, d, 1, 1).data;
                (function(e, t, i) {
                    e /= 255,
                    t /= 255,
                    i /= 255;
                    var o, r, n = Math.max(e, t, i), l = Math.min(e, t, i), c = (n + l) / 2;
                    if (n == l)
                        o = r = 0;
                    else {
                        var a = n - l;
                        switch (r = c > .5 ? a / (2 - n - l) : a / (n + l),
                        n) {
                        case e:
                            o = (t - i) / a + (t < i ? 6 : 0);
                            break;
                        case t:
                            o = (i - e) / a + 2;
                            break;
                        case i:
                            o = (e - t) / a + 4
                        }
                        o /= 6
                    }
                    return {
                        h: Math.round(360 * o),
                        s: Math.round(100 * r),
                        v: Math.round(100 * c)
                    }
                }
                )(n[0], n[1], n[2]),
                c = {
                    r: n[0],
                    g: n[1],
                    b: n[2]
                },
                o = {
                    hex: C(n[0], n[1], n[2])
                },
                r && (l = o.hex ? o.hex : l,
                r.style.backgroundColor = "#" + l,
                u && Q()),
                function(i) {
                    var o = a
                      , n = d;
                    if (k)
                        return void !1;
                    var s = Math.floor(.5 * I)
                      , h = Math.round(o)
                      , p = Math.round(n);
                    if (i) {
                        var f = Z();
                        f.scale(2, 2),
                        f.drawImage(v, .5 * s - h, .5 * s - p),
                        f.scale(.5, .5),
                        f.fillStyle = "rgba(0,0,0,0.3)",
                        f.fillRect(s, 0, 1, I),
                        f.fillRect(0, s, I, 1)
                    } else {
                        var f = Z();
                        f.drawImage(v, -h + s, -p + s);
                        for (var g, A, y = 15, x = 0; x < s; x += 2) {
                            if (g = s - x,
                            A = s + x,
                            f.drawImage(b, A, 0, g, I, A + 1, 0, g, I),
                            f.drawImage(b, 0, 0, g + 1, I, -1, 0, g + 1, I),
                            f.drawImage(b, 0, A, I, g, 0, A + 1, I, g),
                            f.drawImage(b, 0, 0, I, g + 1, 0, -1, I, g + 1),
                            0 == x) {
                                var w = f.getImageData(s, s, 1, 1).data
                                  , C = w[0] + w[1] + w[2];
                                f.fillStyle = C > 192 ? "rgba(30,30,30,0.8)" : "rgba(225,225,225,0.8)"
                            } else
                                f.fillStyle = "rgba(255,255,255,0.4)";
                            for (var R = 0; R < y && !(++x >= s); R++)
                                g = s - x,
                                A = s + x,
                                f.drawImage(b, A, 0, g, I, A + 1, 0, g, I),
                                f.drawImage(b, 0, 0, g + 1, I, -1, 0, g + 1, I),
                                f.drawImage(b, 0, A, I, g, 0, A + 1, I, g),
                                f.drawImage(b, 0, 0, I, g + 1, 0, -1, I, g + 1);
                            --y < 1 && (y = 1),
                            f.fillRect(A + 1, 0, 1, I),
                            f.fillRect(g - 1, 0, 1, I),
                            f.fillRect(0, A + 1, I, 1),
                            f.fillRect(0, g - 1, I, 1)
                        }
                    }
                    t = b.toDataURL();
                    var T = devicePixelRatio > 1 ? 38 : 19
                      , Q = Math.floor(.5 * T)
                      , F = document.createElement("canvas");
                    F.width = T,
                    F.height = T,
                    F.getContext("2d").drawImage(b, s - Q, s - Q, T, T, 0, 0, T, T),
                    F.toDataURL(),
                    S = t,
                    L = l,
                    u || (l = L || l,
                    !E("fehelper-colorpicker-cpimprev") || c && !E("cprgbvl") ? (B(r),
                    e.elemTool.elm("div", {}, [e.elemTool.elm("img", {
                        id: "fehelper-colorpicker-cpimprev",
                        height: 150,
                        width: 150,
                        src: S,
                        style: "margin:0px;padding:0px;margin:0px;"
                    }), e.elemTool.elm("br"), e.elemTool.elm("input", {
                        type: "text",
                        size: 7,
                        style: "width:60px;height:20px;line-height:20px;font-size:10pt;border:" + m,
                        id: "fehelper-colorpicker-cphexvl",
                        value: "#" + l,
                        event: ["mouseover", M]
                    })], r),
                    Y()) : (E("fehelper-colorpicker-cpimprev").src = S,
                    E("fehelper-colorpicker-cpimprev").width = 150,
                    E("fehelper-colorpicker-cpimprev").height = 150,
                    E("fehelper-colorpicker-cphexvl").value = l,
                    r.style.backgroundColor = "#" + l)),
                    !1;
                    var S, L
                }()
            }
        }
        function O() {
            if (s) {
                if (k)
                    return clearTimeout(w),
                    void (w = setTimeout(function() {
                        O()
                    }, 255));
                document.body.style.cursor = "wait",
                k = !0,
                r.style.visibility = "hidden",
                n.style.visibility = "hidden",
                n.src = p;
                var e = innerWidth
                  , t = innerHeight;
                n.style.width = e + "px",
                n.style.height = t + "px",
                setTimeout(function() {
                    try {
                        chrome.runtime.sendMessage({
                            type: "fh-dynamic-any-thing",
                            params: {
                                url: location.href
                            },
                            func: ((e,t)=>{
                                try {
                                    chrome.tabs.query({
                                        active: !0,
                                        currentWindow: !0
                                    }, function(e) {
                                        chrome.tabs.captureVisibleTab(null, {
                                            format: "png"
                                        }, function(t) {
                                            chrome.tabs.executeScript(e[0].id, {
                                                code: "(" + (e=>{
                                                    window.colorpickerNoPage(e)
                                                }
                                                ).toString() + `)(${JSON.stringify({
                                                    setPickerImage: !0,
                                                    pickerImage: t
                                                })})`
                                            })
                                        })
                                    })
                                } catch (e) {}
                                return t && t(null),
                                !0
                            }
                            ).toString()
                        }, function() {})
                    } catch (e) {
                        console.log("有错误发生，可提交此反馈到官网！", e)
                    }
                }, 255)
            }
        }
        function Z() {
            var e = b.getContext("2d");
            return e || (H(),
            b.getContext("2d"))
        }
        function H() {
            (b = document.createElement("canvas")).width = I,
            b.height = I
        }
    }(),
    window.colorpickerNoPage = function(t) {
        e.ColorPicker(t)
    }
}
;
