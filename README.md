# FeHelperLocal

#### 介绍
本插件支持Chrome、Firefox、MS-Edge浏览器，内部工具集持续增加，目前包括 JSON自动/手动格式化、JSON内容比对、代码美化与压缩、信息编解码转换、二维码生成与解码、图片Base64编解码转换、Markdown、 网页油猴、网页取色器、脑图(Xmind)等贴心工具.
本地无法链接外网使用版本，需要搭建https服务器，并需要修改hosts文件
`127.0.0.1 www.baidufe.com`

### 本地访问

普通http请求，预览 `http://localhost/fehelper/插件别名/`

|  插件名称   | 插件别名  |
|  ----  | ----  |
| JSON美化工具  | json-format |
| JSON比对工具  | json-diff |
| 二维码/解码  | qr-code |
| 图片转Base64  | image-base64 |
| 我的便签笔记 | sticky-notes |
| 信息编解码工具 | en-decode |
| 代码美化工具 | code-beautify |
| 代码压缩工具  | code-compress |
| 时间戳转换工具  | timestamp |
| 随机密码生成器  | password |
| Markdown编辑器  | html2markdown |
| 简易版Postman  | postman |
| 正则表达式工具  | regexp |
| 进制转换工具  | trans-radix |
| 颜色转换工具  | trans-color |
| Crontab生成器  | crontab |
| 贷款利率计算器  | loan-rate |
| - | - |
| FH开发者工具  | devtools |
| 网页油猴工具  | page-monkey |
| FeHelper网页截屏工具  | screenshot |
| 页面取色工具  | color-picker |
| FeHelper脑图工具  | naotu |
| FeHelper-网页栅格系统  | grid-ruler |
| 页面性能检测  | page-timing |
| Excel/CVS转JSON  | excel2json |
| - | - |

#### 参考
1. [FeHelper-官网](https://www.baidufe.com/fehelper/index/index.html)
2. [Windows系统配置nginx实现https访问-记录](https://blog.csdn.net/privateobject/article/details/120206612)
