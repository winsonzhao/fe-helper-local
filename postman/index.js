const JSON_SORT_TYPE_KEY = "json_sort_type_key";
new Vue({
    el: "#pageContainer",
    data: {
        urlContent: "",
        methodContent: "GET",
        resultContent: "",
        funcName: "",
        paramContent: "",
        responseHeaders: [],
        jfCallbackName_start: "",
        jfCallbackName_end: "",
        errorMsgForJson: "",
        originalJsonStr: "",
        headerList: [1 * new Date],
        urlencodedDefault: 1
    },
    mounted: function() {
        this.$refs.url.focus()
    },
    methods: {
        postman: function() {
            this.$nextTick(()=>{
                this.sendRequest(this.urlContent, this.methodContent, this.paramContent)
            }
            )
        },
        sendRequest: function(t, e, s) {
            let r = new XMLHttpRequest;
            r.addEventListener("readystatechange", t=>{
                let e = "Loading...";
                switch (t.target.readyState) {
                case t.target.OPENED:
                    e = "Senting...";
                    break;
                case t.target.HEADERS_RECEIVED:
                    e = "Headers received",
                    this.responseHeaders = t.target.getAllResponseHeaders().trim().split("\n").map(t=>t.split(": ").map(t=>t.trim()));
                    break;
                case t.target.LOADING:
                    e = "Loading...";
                    break;
                case t.target.DONE:
                    try {
                        e = JSON.stringify(JSON.parse(t.target.responseText), null, 4)
                    } catch (s) {
                        e = t.target.responseText
                    }
                    this.jsonFormat(e),
                    this.renderTab()
                }
                this.resultContent = e || "无数据"
            }
            ),
            r.open(e, t);
            let a = !1;
            "post" === e.toLowerCase() && (a = !0,
            this.urlencodedDefault && r.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")),
            this.headerList.forEach(t=>{
                let e = $(`#header_key_${t}`).val()
                  , s = $(`#header_value_${t}`).val();
                e && s && r.setRequestHeader(e, s)
            }
            ),
            r.send(a && s)
        },
        addHeader() {
            this.headerList.push(1 * new Date)
        },
        deleteHeader(t) {
            t.target.parentNode.remove()
        },
        renderTab: function() {
            jQuery("#tabs").tabs({
                show: (t,e)=>{}
            }),
            this.$refs.resultContainer.classList.remove("hide")
        },
        jsonFormat: function(t) {
            if (this.errorMsgForJson = "",
            this.jfCallbackName_start = "",
            this.jfCallbackName_end = "",
            !t)
                return !1;
            let e = null;
            try {
                this.funcName = "";
                let s = /^([\w\.]+)\(\s*([\s\S]*)\s*\)$/gim.exec(t);
                null != s && (this.funcName = s[1],
                t = s[2]),
                e = JSON.parse(t)
            } catch (s) {
                try {
                    e = new Function("return " + t)()
                } catch (s) {
                    try {
                        "string" == typeof (e = new Function("return '" + t + "'")()) && (e = new Function("return " + e)())
                    } catch (t) {
                        this.errorMsgForJson = t.message
                    }
                }
            }
            if (null != e && "object" == typeof e && !this.errorMsgForJson.length) {
                try {
                    t = JSON.stringify(e)
                } catch (t) {
                    this.errorMsgForJson = t.message
                }
                if (!this.errorMsgForJson.length) {
                    this.originalJsonStr = t;
                    let e = parseInt(localStorage.getItem(JSON_SORT_TYPE_KEY) || 0);
                    this.didFormat(e),
                    $("[name=jsonsort][value=" + e + "]").attr("checked", 1);
                    let s = this;
                    $("[name=jsonsort]").click(function(t) {
                        let r = parseInt(this.value);
                        r !== e && (s.didFormat(r),
                        e = r),
                        localStorage.setItem(JSON_SORT_TYPE_KEY, r)
                    })
                }
            }
            if (this.errorMsgForJson) {
                let t = document.querySelector("#optionBar");
                t && (t.style.display = "none")
            }
        },
        didFormat: function(t) {
            t = t || 0;
            let e = this.originalJsonStr;
            if (0 !== t) {
                let s = JsonABC.sortObj(JSON.parse(this.originalJsonStr), parseInt(t), !0);
                e = JSON.stringify(s)
            }
            Formatter.format(e),
            $(".x-toolbar").fadeIn(500),
            this.funcName ? ($("#jfCallbackName_start").html(this.funcName + "("),
            $("#jfCallbackName_end").html(")")) : (this.jfCallbackName_start = "",
            this.jfCallbackName_end = "")
        },
        setDemo: function(t) {
            1 === t ? (this.urlContent = "http://t.weather.sojson.com/api/weather/city/101030100",
            this.methodContent = "GET") : (this.urlContent = "https://www.baidufe.com/test-post.php",
            this.methodContent = "POST",
            this.paramContent = "username=postman&password=123456")
        }
    }
});
