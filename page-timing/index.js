new Vue({
    el: "#pageContainer",
    data: {
        pageTitle: "无",
        pageUrl: "无",
        timing: null,
        headerInfo: null,
        tmDefination: {
            lookupDomainTime: "DNS查询耗时",
            connectTime: "TCP连接耗时",
            requestTime: "网络请求耗时",
            firstPaintTime: "白屏时间",
            readyStart: "构建文档流耗时",
            domReadyTime: "DOM树构建耗时",
            redirectTime: "重定向耗时",
            appcacheTime: "数据缓存耗时",
            unloadEventTime: "卸载文档耗时",
            initDomTreeTime: "请求完成到可交互",
            loadEventTime: "加载事件耗时",
            loadTime: "加载总耗时"
        }
    },
    mounted: function() {
        "chrome-extension:" === location.protocol && chrome.runtime.onMessage.addListener((e,t,i)=>("TAB_CREATED_OR_UPDATED" === e.type && e.content && e.event === location.pathname.split("/")[1] && (sessionStorage.setItem("wpo-data", JSON.stringify(e.content)),
        this.showTiming(e.content)),
        i && i(),
        !0));
        let e = JSON.parse(sessionStorage.getItem("wpo-data"));
        e && this.showTiming(e)
    },
    methods: {
        showTiming(e) {
            this.pageTitle = e.pageInfo.title || "无",
            this.pageUrl = e.pageInfo.url || "无",
            this.timing = e.time,
            this.headerInfo = e.header
        }
    }
});
