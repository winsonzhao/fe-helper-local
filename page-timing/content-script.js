window.pagetimingContentScript = function() {
    let __importScript = filename=>{
        fetch(filename).then(t=>t.text()).then(jsText=>eval(jsText))
    }
    ;
    !function(t) {
        "use strict";
        t.timing = t.timing || {
            getTimes: function(e) {
                var i = t.performance || t.webkitPerformance || t.msPerformance || t.mozPerformance;
                if (void 0 === i)
                    return !1;
                var n, o = i.timing, r = {};
                if (e = e || {},
                o) {
                    if (e && !e.simple)
                        for (var a in o)
                            n = o[a],
                            !isNaN(parseFloat(n)) && isFinite(n) && (r[a] = parseFloat(o[a]));
                    if (void 0 === r.firstPaint) {
                        var m = 0;
                        if ("number" == typeof o.msFirstPaint)
                            m = o.msFirstPaint,
                            r.firstPaintTime = m - o.navigationStart;
                        else if (void 0 !== i.getEntriesByName) {
                            var d = i.getEntriesByName("first-paint");
                            if (1 === d.length) {
                                var c = d[0].startTime;
                                m = i.timeOrigin + c,
                                r.firstPaintTime = c
                            }
                        }
                        e && !e.simple && (r.firstPaint = m)
                    }
                    r.loadTime = o.loadEventEnd - o.fetchStart,
                    r.domReadyTime = o.domComplete - o.domInteractive,
                    r.readyStart = o.fetchStart - o.navigationStart,
                    r.redirectTime = o.redirectEnd - o.redirectStart,
                    r.appcacheTime = o.domainLookupStart - o.fetchStart,
                    r.unloadEventTime = o.unloadEventEnd - o.unloadEventStart,
                    r.lookupDomainTime = o.domainLookupEnd - o.domainLookupStart,
                    r.connectTime = o.connectEnd - o.connectStart,
                    r.requestTime = o.responseEnd - o.requestStart,
                    r.initDomTreeTime = o.domInteractive - o.responseEnd,
                    r.loadEventTime = o.loadEventEnd - o.loadEventStart
                }
                return r
            },
            printTable: function(t) {
                var e = {}
                  , i = this.getTimes(t) || {};
                Object.keys(i).sort().forEach(function(t) {
                    e[t] = {
                        ms: i[t],
                        s: +(i[t] / 1e3).toFixed(2)
                    }
                }),
                console.table(e)
            },
            printSimpleTable: function() {
                this.printTable({
                    simple: !0
                })
            }
        },
        "undefined" != typeof module && module.exports && (module.exports = t.timing)
    }("undefined" != typeof window ? window : {});
    let DetectMgr = (()=>{
        let t = {
            pageInfo: {
                title: document.title,
                url: location.href
            },
            time: window.timing.getTimes({
                simple: !0
            })
        }
          , e = function() {
            chrome.runtime.sendMessage({
                type: "fh-dynamic-any-thing",
                params: {
                    tabId: window.__FH_TAB_ID__ || null,
                    wpoInfo: t
                },
                func: ((t,e)=>(chrome.DynamicToolRunner({
                    query: "tool=page-timing",
                    withContent: t.wpoInfo
                }),
                e && e(),
                !0)).toString()
            })
        };
        return {
            detect: function() {
                /^((http)|(https)):\/\//.test(location.href) ? t.header && t.time && t.pageInfo ? e() : fetch(location.href).then(t=>{
                    let e = {};
                    for (let i of t.headers.entries())
                        e[i[0]] = i[1];
                    return e
                }
                ).then(i=>{
                    t.header = i,
                    e()
                }
                ).catch(console.log) : e()
            }
        }
    }
    )();
    window.pagetimingNoPage = function() {
        DetectMgr.detect()
    }
}
;
